// This widget can be obtained at https://bitbucket.org/xmillis/madeinromania/overview

var WIDGET_SIZE="hdpi"; // can be hdpi or xhdpi
var ROMANIA_SIZE="xx"+WIDGET_SIZE;
var ROMANIA_PROPORTION=0.8;
var IN_PROPORTION=0.48;
var SCULPTURE_PROPORTION=0.55;
var ROCKET_PROPORTION=0.75;
var PEN_PROPORTION=0.65;
var MAP_PROPORTION=0.9;
var DRACULA_PROPORTION=0.7;
var PANFLUTE_PROPORTION=0.48;
var MAP_SIZE=WIDGET_SIZE;

/**
 * Call this to insert the widget
 * @param showIcons True if icons should be displayed
 * @param pathToWidget The relative path to the widget folder (including it but not entering it) from the current browser location
 */
function madeInRomania(showIcons,pathToWidget) {
	if (typeof pathToWidget === 'undefined'){
		pathToWidget='../madeinromania';
	}
	var map="<a href='https://en.wikipedia.org/wiki/Romania' target='_blank' title='Romania - incubator and hub for a technology-driven future'><img src='"+pathToWidget+"/images/map/"+MAP_SIZE+".png' style='"+getProportion(MAP_PROPORTION)+"' /></a>";
	var rocket="<a href='https://en.wikipedia.org/wiki/Coand%C4%83_effect' target='_blank' title='Henri Coanda - the aerodynamics pioneer'><img src='"+pathToWidget+"/images/icons/rocket/"+WIDGET_SIZE+".png' style='margin-left:-29.5%;margin-bottom:-1%;"+getProportion(ROCKET_PROPORTION)+"'/></a>";
	var gymnast="<a href='https://en.wikipedia.org/wiki/Perfect_10_(gymnastics)' target='_blank' title='Nadia Comaneci - first perfect 10 in gymnastics'><img src='"+pathToWidget+"/images/icons/gymnast/"+WIDGET_SIZE+".png' style='margin-left:"+mL+"%;"+getProportion(SCULPTURE_PROPORTION)+"'/></a>";
	var sculpture="<a href='https://en.wikipedia.org/wiki/Constantin_Br%C3%A2ncu%C8%99i' target='_blank' title='Brancusi - \"patriarch of modern sculpture\"'><img src='"+pathToWidget+"/images/icons/sculpture/"+WIDGET_SIZE+".png' style='margin-left:-8.5%;"+getProportion(SCULPTURE_PROPORTION)+"'/></a>";
	var made="<img src='"+pathToWidget+"/images/text/made/"+WIDGET_SIZE+".png' style='margin-left:-2%;'/>";
	var _in="<img src='"+pathToWidget+"/images/text/in/"+WIDGET_SIZE+".png' style='margin-left:-11%;"+getProportion(IN_PROPORTION)+"'/>";
	var romania="<img src='"+pathToWidget+"/images/text/romania/"+ROMANIA_SIZE+".png' style='margin-left:-16.5%;margin-top:"+mT+"%;margin-bottom:-12.5%;"+getProportion(ROMANIA_PROPORTION)+"+'/>";
	var pen="<a href='https://www.google.ro/search?q=inventor+of+the+fountain+pen' target='_blank' title='Petrache Poenaru - inventor of the fountain pen'><img src='"+pathToWidget+"/images/icons/fountain-pen/"+WIDGET_SIZE+".png' style='margin-left:-5.5%;margin-bottom:-0.75%;"+getProportion(PEN_PROPORTION)+"'/></a>";
	var dracula="<a href='https://en.wikipedia.org/wiki/Vlad_the_Impaler' target='_blank' title='Vlad Dracula The Impaler - inspired the legend of Count Dracula'><img src='"+pathToWidget+"/images/icons/dracula/"+WIDGET_SIZE+".png' style='margin-left:-7%;margin-bottom:-0.5%;"+getProportion(DRACULA_PROPORTION)+"'/>";
	var panflute="<a href='https://www.youtube.com/watch?v=d6ajKT9xzvU' target='_blank' title='Gheorghe Zamfir - the legendary pan flute musician'><img src='"+pathToWidget+"/images/icons/panflute/"+WIDGET_SIZE+".png' style='margin-left:-4.5%;margin-bottom:-1%;"+getProportion(PANFLUTE_PROPORTION)+"'/></a>";
	var html = "<table><tr><td><table style='min-height:160px'><tr><td>"+map+"</td><td>"+made+_in+romania+"</td></tr></table></td><td>"+(showIcons?(rocket+gymnast+sculpture+dracula+panflute+pen):"")+"</td></table>";
	document.write(html);
}

function getProportion(proportion){
	return "transform: scale("+proportion+", "+proportion+");-ms-transform: scale("+proportion+", "+proportion+");-webkit-transform: scale("+proportion+", "+proportion+");";
}

var isFF = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
var mT = isFF ? -37.5 : -87.5;
var mL = isFF ? -19 : -8;