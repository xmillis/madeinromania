## Usage

See an example of the widget's use at https://currentmillis.com/mission/

1. Copy the "madeinromania" folder to your website.

2. Just include the script: `<script type="text/javascript" src="widget.js"></script>`

3. and call `<script>madeInRomania(true)</script>` wherever you want the widget to be included in your html

---

## Specification

Mock-up:

1. "Made In": font Viga, padding 35%, color FF2D07 and FFA41B, dimension web

2. "Romania": font Viga, padding  0%, color 1BA3D5, dimension web

3. "Romania" shape background: padding 25%, color FFA41B, background 1BA3D5, dimension web

Widget resources:

1. "Romania" shape background: padding: 25%, color FFA41B, background 1BA3D5, dimension all

2. "Made" font: Viga, padding 0%, color FF2D07, dimension all

3. "In" font: Viga, padding 0%, color FFA41B, dimension all

4. "Romania": font Viga, padding 0%, color 1BA3D5, dimension all

5. Gymnastics icon: padding 0%, color 669900, dimension all

6. Rocket (Coanda effect) icon: padding 5%, color 669900, dimension all

7. Sculpture icon: padding 0%, color 669900, dimension all

8. Dracula icon: padding 0%, color 669900, dimension all

10. Panflute icon: padding 0%, color 669900, dimension all

11. Fountain-pen icon: padding 0%, color 669900, dimension all

---

## Licenses

1. "Viga" font used under the Open Font License

2. "Romania" shape used (as raster) under GNU FDL / CC BY-SA 3.0 Unported from https://commons.wikimedia.org/wiki/File:EU-Romania.svg

3. Gymnastics icon used (as raster) under GNU FDL / CC BY-SA 3.0 from https://commons.wikimedia.org/wiki/File:Gymnastics128px.png Attribution: Merosonox at the English language Wikipedia

4. Rocket exhaust icon used (as raster) under CC0 (public domain) from https://pixabay.com/en/rocket-missile-launch-military-2289803/

5. Dracula icon used (as raster) under CC0 (public domain) from https://pixabay.com/en/bat-black-dracula-wings-spread-151366/

6. Panflute icon used (as raster) under CC BY-SA 3.0 from https://commons.wikimedia.org/wiki/File:Panpipes.svg

7. Fountain pen icon used (as raster) as "free"under CC0 (public domain) from https://commons.wikimedia.org/wiki/File:Wriging_Nib_Black_Down_Left.svg

8. Final versions of the icons created using Android Asset Studio: https://romannurik.github.io/AndroidAssetStudio/index.html

---

## Source Code

1. Is modeled as a general Eclipse project.